// Fill out your copyright notice in the Description page of Project Settings.

#include "TankPlayerController.h"
#include "Battletank.h"
#include "Engine/World.h"
#include "GameFramework/Actor.h"		//For being able to use GetOwner()
#include "GameFramework/PlayerController.h"
#include "TankAimingComponent.h"
#include "Engine/LocalPlayer.h"
#include "Tank.h"

void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();
	AimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	if (ensure(AimingComponent))
	{
		FoundAimingComponent(AimingComponent);
	}
}

void ATankPlayerController::Tick(float DeltaTime)	//Because TankPlayerController is part of the class ATankPLayerController, we have to add that in the definition
{
	Super::Tick(DeltaTime);
	AimTowardsCrosshair();
}

void ATankPlayerController::AimTowardsCrosshair()
{
	if (!GetPawn()) { return; }

	auto AimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	if (!ensure(AimingComponent)) { return; }

	FVector	OUTHitLocation; //Out parameter
	bool Collision = GetLookVectorHitLocation(OUTHitLocation);
	AimingComponent->AimAt(OUTHitLocation, Collision);

}

//Find the crosshair position in pixel coordinates
FVector2D ATankPlayerController::GetCrosshairScreenLocation() const
{
	int32 ViewportSizeX, ViewportSizeY;
	GetViewportSize(ViewportSizeX, ViewportSizeY);
	FVector2D CrosshairScreenLocation(ViewportSizeX * CrosshairXLocation, ViewportSizeY * CrosshairYLocation);
	return CrosshairScreenLocation;
}

//Function that returns me the rotation of the crosshair in world coordinates
bool ATankPlayerController::GetLookDirection(FVector& LookDirection) const
{
	auto CrosshairScreenLocation = GetCrosshairScreenLocation();

	//"De-project" the screen position of the crosshair to a world rotation
	FVector WorldLocation;	//WorldLocation is a useless parameter (in this case) that we won't use but we have to put
	return DeprojectScreenPositionToWorld(CrosshairScreenLocation.X, CrosshairScreenLocation.Y, WorldLocation, LookDirection);	//returns true if it works, modifies lookdirection as
																												//out parameter
}

//Function that returns me the location of the aimed point
bool ATankPlayerController::GetLookVectorHitLocation(FVector& OutHitLocation) const
{
	auto CrosshairScreenLocation = GetCrosshairScreenLocation();

	FVector LookDirection;
	if (GetLookDirection(LookDirection))		//I start by getting the direction I'm looking at. If it's true I do the rest
	{
		FHitResult OutHit;		//Out parameter to store the result of the hit/collision of the ray
		///Setup query parameters
		FCollisionQueryParams TraceParameters(FName(TEXT("")), false, GetOwner());

		auto StartLocation = PlayerCameraManager->GetCameraLocation();	//Gets the camera of the player
		FVector End = StartLocation + LookDirection * LineTraceRange;

		bool hit = GetWorld()->LineTraceSingleByChannel(
			OutHit,
			StartLocation,
			End,
			ECollisionChannel::ECC_Camera,
			TraceParameters
		);

		if (hit)
		{
			OutHitLocation = OutHit.Location;			//Obtain a vector with the location of the hit
		}
		else
		{
			//If there is no collision in line, I move towards a point in space made by the camera
			FVector CameraLocation;
			FVector CameraDirection;
			DeprojectScreenPositionToWorld(CrosshairScreenLocation.X, CrosshairScreenLocation.Y, CameraLocation, CameraDirection);
			OutHitLocation = FVector(CameraLocation + CameraDirection * 1000000.f);
		}

		return hit;									//Return the result of if the ray hits something or not (true or false values)
	}
	else
	{
		OutHitLocation = FVector(0);				//Hit vector 0 if no hit
		return false;								//If I fail to obtain DeprojectScreenPositionToWorld, I count it as a fail of hit 
	}
}

void ATankPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto PossessedTank = Cast<ATank>(InPawn);
		if (!ensure(PossessedTank)) { return; }

		PossessedTank->OnDeath.AddUniqueDynamic(this, &ATankPlayerController::OnTankDeath);
	}
}

void ATankPlayerController::OnTankDeath()
{
	StartSpectatingOnly();
}