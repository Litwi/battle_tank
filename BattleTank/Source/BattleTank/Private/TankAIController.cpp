// Fill out your copyright notice in the Description page of Project Settings.


#include "TankAIController.h"
#include "Engine/World.h"
#include "TankMovementComponent.h"
#include "TankAimingComponent.h"
#include "Battletank.h"
#include "Tank.h"

//Depends on movement component via pathfiinding system

void ATankAIController::BeginPlay()
{
	Super::BeginPlay();
	AimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
}

void ATankAIController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	auto PlayerPawn = GetWorld()->GetFirstPlayerController()->GetPawn();
	auto AITank = GetPawn();
	if (!ensure(PlayerPawn && AITank)) { return; }

	//Move towards the player
	MoveToActor(PlayerPawn, AceptanceRadius);

	AimingComponent->AimAt(PlayerPawn->GetActorLocation(), true);		//The true marks that there is collision (AITanks will always aim towards the player and not the sky)

	//Fire
	if (AimingComponent->GetFiringStatus() == EFiringStatus::Locked)
	{
		AimingComponent->Fire();
	}
}

void ATankAIController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto PossessedTank = Cast<ATank>(InPawn);
		if (!ensure(PossessedTank)) { return; }

		PossessedTank->OnDeath.AddUniqueDynamic(this, &ATankAIController::OnTankDeath);
	}
}

void ATankAIController::OnTankDeath()
{
	if(!ensure(GetPawn())){ return; }
	GetPawn()->DetachFromControllerPendingDestroy();
}
