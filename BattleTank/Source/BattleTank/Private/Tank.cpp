#include "Tank.h"
#include "BattleTank.h"



// Sets default values
ATank::ATank()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
}

void ATank::BeginPlay()
{
	Super::BeginPlay();
	CurrentHealth = MaxHealth;
}

//Called automatically from ApplyRadialDamage in projectile
float ATank::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	int32 DamagePoints = FPlatformMath::RoundToInt(Damage);
	auto DamageToApply = FMath::Clamp(DamagePoints, 0, CurrentHealth);

	CurrentHealth -= DamageToApply;

	if (CurrentHealth <= 0)
	{
		OnDeath.Broadcast();
		//this->Destroy();
	}

	return DamageToApply;
}

float ATank::GetHealthPercent() const
{
	auto value = (float)CurrentHealth / (float)MaxHealth;
	UE_LOG(LogTemp, Warning, TEXT("%f"), value)
	return value;
	
}