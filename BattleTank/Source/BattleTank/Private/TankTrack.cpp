// Fill out your copyright notice in the Description page of Project Settings.


#include "TankTrack.h"
#include "BattleTank.h"
#include "SprungWheel.h"
#include "SpawnPoint.h"

UTankTrack::UTankTrack()
{
	PrimaryComponentTick.bCanEverTick = false;
}

//void UTankTrack::BeginPlay()
//{
//	Super::BeginPlay();
//	OnComponentHit.AddDynamic(this, &UTankTrack::OnHit);
//}

void UTankTrack::SetThrottle(float Throttle)
{
	float CurrentThrottle = FMath::Clamp<float>(Throttle, -1, 1);
	DriveTrack(CurrentThrottle);
}

void UTankTrack::DriveTrack(float CurrentThrottle)
{
	auto ForceApplied = CurrentThrottle * TrackMaxDrivingForce;
	auto Wheels = GetWheels();
	auto ForcePerWheel = ForceApplied / Wheels.Num();
	for (ASprungWheel* Wheel : Wheels)
	{
		Wheel->AddDrivingForce(ForcePerWheel);
	}
}

TArray<class ASprungWheel*> UTankTrack::GetWheels() const
{
	
	TArray<USceneComponent*> SpawnPointArray;
	TArray<ASprungWheel*> WheelArray;

	GetChildrenComponents(true, SpawnPointArray);	//Give all children of the track (only spawnpoints for the moment)

	for (USceneComponent* SpawnPoint : SpawnPointArray)
	{
		auto SpawnPointChild = Cast<USpawnPoint>(SpawnPoint);	//Cast to USpawnPoint to make sure it's that type
		if (!ensure(SpawnPointChild)) { continue; }
		
		AActor* SpawnedChild = SpawnPointChild->GetSpawnedActor();	//I get the actor spawned by spawnPoint which is sprungwheel in this case
		auto SprungWheel = Cast<ASprungWheel>(SpawnedChild);		//Cast to ASprungWheel to make sure it's that type
		if (!ensure(SprungWheel)) { continue; }

		WheelArray.Add(SprungWheel);
	}
	
	return WheelArray;
}

//void UTankTrack::AppliedSidewaysForce()
//{
//	auto SlippageSpeed = FVector::DotProduct(GetRightVector(), GetComponentVelocity());
//
//	auto DeltaTime = GetWorld()->GetDeltaSeconds();
//	auto CorrectionAcceleration = -SlippageSpeed / DeltaTime * GetRightVector();
//
//	auto TankRoot = Cast<UPrimitiveComponent>(GetOwner()->GetRootComponent());
//	auto CorrectionForce = CorrectionAcceleration * TankRoot->GetMass() / 2;
//	auto ForceLocation = GetComponentLocation();
//
//	TankRoot->AddForce(CorrectionForce);
//}

//void UTankTrack::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
//{
//	AppliedSidewaysForce();
//	
//	CurrentThrottle = 0;
//}