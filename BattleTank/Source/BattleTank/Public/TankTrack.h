 // Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTrack.generated.h"


UCLASS(meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankTrack : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BluePrintCallable, Category = Input)
		void SetThrottle(float Throttle);

	//Max force per track in Newtons
	UPROPERTY(EditAnywhere, Category = Setup)
		float TrackMaxDrivingForce = 80000000;

private:
	UTankTrack();

	//virtual void BeginPlay() override;

	void DriveTrack(float CurrentThrottle);

	//void AppliedSidewaysForce();

	TArray<class ASprungWheel*> GetWheels() const;

	/*UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);*/
};
