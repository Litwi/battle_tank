#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TankAimingComponent.generated.h"

class UTankBarrel;
class UTankTurret;
class AProjectile;

UENUM()
enum class EFiringStatus : uint8
{
	Reloading,
	Aiming,
	Locked,
	NoAmmo
};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLETANK_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	void AimAt(FVector HitLocation, bool Collision);

	UFUNCTION(BlueprintCallable, Category = Setup)
		void Initialise(UTankBarrel* BarrelToSet, UTankTurret* TurretToSet);

	UFUNCTION(BlueprintCallable, Category = Firing)
		void Fire();

	EFiringStatus GetFiringStatus() const;

	UFUNCTION(BlueprintCallable, Category = UI)
		int32 GetAmmo() const;						//TODO create ammo recharging points

protected:
	//I put this here because I'm calling it from a subclass which is TankPlayerController_BP
	UPROPERTY(BlueprintReadOnly, Category = State)
		EFiringStatus FiringStatus = EFiringStatus::Reloading;

private:
	// Sets default values for this component's properties
	UTankAimingComponent();

	virtual void BeginPlay() override;

	virtual void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void MoveBarrel(FVector AimDirection);	//Method to move the barrel

	bool IsBarrelMoving();

	UTankBarrel* Barrel = nullptr;		//Barrel is set in beginplay from the blueprint
	UTankTurret* Turret = nullptr;		//Turret is set in beginplay from the blueprint

	UPROPERTY(EditDefaultsOnly, Category = Firing)
		float LaunchSpeed = 8500;							//Sets the launch speed of the projectile (and hence the reach)
	UPROPERTY(EditAnywhere, Category = Firing)
		float ReloadTimeSeconds = 3;
	UPROPERTY(EditDefaultsOnly, Category = Firing)
		int32 Ammo = 10;
	UPROPERTY(EditDefaultsOnly, Category = Setup)
		TSubclassOf<AProjectile> ProjectileBlueprint;		//Creation of a projectile subclassin BP

	double LastFireTime = 0;

	FVector AimDirection;
};
