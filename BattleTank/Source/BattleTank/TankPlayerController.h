#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"		//Must be the last include in the list

class ATank;
class UTankAimingComponent;

UCLASS()
class BATTLETANK_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	virtual void Tick(float DeltaTime) override;

	void BeginPlay() override;

protected:
	//Needs to be in protected to be blueprintcallable

	UFUNCTION(BlueprintImplementableEvent, Category = Setup)		//Defined in blueprint
		void FoundAimingComponent(UTankAimingComponent* AimingCompRef);

private:
	//Start the tank moving the barrel so the shot hits where the crosshair intersects with the world
	void AimTowardsCrosshair();

	FVector2D GetCrosshairScreenLocation() const;

	bool GetLookDirection(FVector&) const;

	bool GetLookVectorHitLocation(FVector& OutHitLocation) const;

	virtual void SetPawn(APawn* InPawn) override;

	UFUNCTION()
		void OnTankDeath();

	UPROPERTY(EditDefaultsOnly)
		float CrosshairXLocation = 0.5;
	UPROPERTY(EditDefaultsOnly)
		float CrosshairYLocation = 0.3333;
	UPROPERTY(EditDefaultsOnly)
		float LineTraceRange = 999999;

	UTankAimingComponent* AimingComponent = nullptr;
};
